# Git rebase practice

# Excercise 1
Create two feature branches from master, then add changes to the first branch and merge them into master. Rebase the second branch onto master so it will have the changes from the first branch.

## Initial state
```
           feature-2
           O----O----O
          /
O----O----O
master    \
           O----O----O
           feature-1
```
## After merging feature-1 into master
```
           feature-2
           O----O----O
          /     
O----O----O----O----O
master
```
## After rebasing feature-2 onto master
```
                     feature-2
                     O----O----O
                    /
O----O----O----O----O
master
```

# Excercise 2
Add 3 commits to your feature-2 branch, make sure the second commit is a fix for a mistake in the first commit. Use **git rebase -i HEAD~3** to clean up the last 3 commits. While on the interactive rebase, squash the second commit into the first one.

# Excercise 3
Merge the changes from feature-2 branch into master and checkout feature-1 branch. Add changes in feature-1 branch that will conflict with the ones that you just merged into master from feature-2 branch. Rebase feature-1 branch onto master and resolve the conflicts.

